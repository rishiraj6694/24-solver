# 24 Solver

24 is a [popular card game](https://www.amazon.com/24-Game-Single-Digit-cards/dp/B002AODZFQ) where the objective is to find a way to combine four whole numbers so that the end result is 24. The allowed operations for combining the numbers are addition, subtraction, multiplication, and division. 

This Python program finds a solution (if any exists) to the general problem: How to reach a specified target number with a specified set of starting numbers?

# Some sample runs

![](Media/24-solver.png)

![](Media/24-solver2.png)