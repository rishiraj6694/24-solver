
checked = {}
numOperations = 4

def combine(a, b, i):
    if i == 0:
        return a + b
    elif i == 1:
        return a * b
    elif i == 2:
        return max(a,b) - min(a,b)
    elif i == 3:
        return min(a,b) - max(a,b)

def toString(a, b, i):
    if i==0:
        return str(a) + " + " + str(b)
    elif i==1:
        return str(a) + " * " + str(b)
    elif i==2:
        return str(max(a,b)) + " - " + str(min(a,b))
    elif i==3:
        return str(min(a,b)) + " - " + str(max(a,b))

def findSolution(nums, target):
    nums = tuple(sorted(nums))
    checked[tuple(nums)] = True
    if len(nums) == 2:
        a, b = nums[0], nums[1]
        for i in range(numOperations):
            if combine(a, b, i) == target:
                print(nums, "->", toString(a, b, i))
                return True
        return False
    elif len(nums) >= 2:
        for i in range(0, len(nums) - 1):
            for j in range(i+1, len(nums)):
                a, b = nums[i], nums[j]
                combined = [combine(a, b, n) for n in range(numOperations)]
                newNums = [[nums[k] for k in range(len(nums)) if k != i and k != j]
                           for n in range(numOperations)]
                for n in range(numOperations):
                    newNums[n].append(combined[n])
                    if tuple(sorted(newNums[n])) not in checked:
                        if findSolution(newNums[n], target):
                            print(nums, "->", toString(a, b, n))
                            return True
        return False

while True:
    print()
    target = int(input("Choose target: "))
    numbers = input("List starting numbers, separated by spaces: ").split(" ")
    nums = [int(n) for n in numbers]
    indent = len(nums)
    solution = findSolution(nums, target)
    if solution:
        solution
    else:
        print("No solution")


